<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['fio', 'phone', 'article', 'sum', 'status'];

    const STATUS_NEW = 1;
    const STATUS_MANAGER = 2;
    const STATUS_COURIER = 3;
    const STATUS_DELIVERED = 4;
    const STATUS_DENIED = 5;

    public static $statuses = [
        self::STATUS_NEW, self::STATUS_MANAGER, self::STATUS_COURIER,
        self::STATUS_DELIVERED, self::STATUS_DENIED
    ];
    public static $statuses_translations = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_MANAGER => 'Позвонил менеджер',
        self::STATUS_COURIER => 'Доставка курьером',
        self::STATUS_DELIVERED => 'Доставлено',
        self::STATUS_DENIED => 'Клиент отказался',
    ];

    const ARTIKUL_HP = 'hp';
    const ARTIKUL_CANON = 'canon';
    const ARTIKUL_XEROX = 'xerox';

    public static $articles = [
        self::ARTIKUL_HP, self::ARTIKUL_CANON, self::ARTIKUL_XEROX
    ];

    public static $prices = [
        self::ARTIKUL_CANON => 1000,
        self::ARTIKUL_HP => 2000,
        self::ARTIKUL_XEROX => 3000,
    ];

    public function cartridges()
    {
        return $this->hasMany(Cartridge::class, 'order_id', 'id');
    }

    public function getStatusHumanAttribute()
    {
        if (isset(self::$statuses_translations[$this->status])) {
            return self::$statuses_translations[$this->status];
        }
    }
}
