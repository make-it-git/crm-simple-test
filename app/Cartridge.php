<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartridge extends Model
{
    protected $table = 'order_cartridges';
    protected $fillable = ['type'];
    public $timestamps = false;

    const TYPE_CANON_CNB = 'cnb';
    const TYPE_CANON_CNBXL = 'cnbxl';

    const TYPE_HP_HPR = 'hpr';
    const TYPE_HP_HPB = 'hpb';
    const TYPE_HP_HPG = 'hpg';
    const TYPE_HP_HPP = 'hpp';

    public static $prices = [
        Order::ARTIKUL_HP => [
            self::TYPE_HP_HPR => 390,
            self::TYPE_HP_HPB => 391,
            self::TYPE_HP_HPG => 392,
            self::TYPE_HP_HPP => 393,
        ],
        Order::ARTIKUL_CANON => [
            self::TYPE_CANON_CNB => 390,
            self::TYPE_CANON_CNBXL => 490,
        ],
        Order::ARTIKUL_XEROX => [
        ],
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
}
