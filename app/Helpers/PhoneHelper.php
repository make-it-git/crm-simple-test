<?php

namespace app\Helpers;

class PhoneHelper
{
    protected $phone;

    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    public function isValid()
    {
        $phone = preg_replace('/\D/', '', $this->phone);
        return preg_match("/^\d{11}$/", $phone);
    }

    public function clean()
    {
        return preg_replace('/\D/', '', $this->phone);
    }
}
