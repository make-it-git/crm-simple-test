<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use App\Helpers\PhoneHelper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('valid_phone', function($attribute, $value, $parameters) {
            return (new PhoneHelper($value))->isValid();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
