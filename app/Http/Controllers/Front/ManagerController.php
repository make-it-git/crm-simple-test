<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Http\Requests\Front\OrderUpdateRequest;

class ManagerController extends Controller
{
    public function index()
    {
        $orders = Order::latest()->paginate(10);
        return view('front.manager.orders', compact('orders'));
    }

    public function order(Order $order)
    {
        return view('front.manager.order-modal', compact('order'));
    }

    public function updateOrder(OrderUpdateRequest $request, Order $order)
    {
        $order->fill($request->only(['status', 'fio', 'phone']));
        $order->save();
        $orders = Order::latest()->paginate(10);
        return view('front.manager.orders-table', compact('orders'));
    }
}
