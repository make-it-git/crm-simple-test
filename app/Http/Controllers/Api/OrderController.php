<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderCreateRequest;
use App\Order;
use App\Cartridge;
use App\Helpers\PhoneHelper;

class OrderController extends Controller
{
    public function create(OrderCreateRequest $request)
    {
        $printerPrice = Order::$prices[$request->article];
        $cartridgesTotalPrice = $request->sum - $printerPrice;
        if ($cartridgesTotalPrice < 0) {
            return response()->json([
                'id' => -1
            ]);
        }
        $cartridges = [];
        if ($cartridgesTotalPrice > 0) {
            $cartridgePrices = Cartridge::$prices[$request->article];
            foreach ($cartridgePrices as $type => $price) {
                if ($price <= $cartridgesTotalPrice && $cartridgesTotalPrice > 0) {
                    $cartridges[] = $type;
                    $cartridgesTotalPrice -= $price;
                }
            }
            if ($cartridgesTotalPrice != 0) {
                return response()->json([
                    'id' => -1
                ]);
            }
        }

        $data = $request->all();
        $data['status'] = Order::STATUS_NEW;
        $data['phone'] = (new PhoneHelper($data['phone']))->clean();
        $order = Order::create($data);
        foreach ($cartridges as $type) {
            $order->cartridges()->save(new Cartridge(['type' => $type]));
        }
        return response()->json([
            'id' => $order->id
        ]);
    }
}
