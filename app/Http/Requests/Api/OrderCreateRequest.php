<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use App\Order;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio' => 'required|string|max:256',
            'phone' => 'required|string|valid_phone',
            'article' => 'required|in:' . join(',',  Order::$articles),
            'sum' => 'required|integer|min:1'
        ];
    }
}
