<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fio');
            $table->bigInteger('phone')->unsigned();
            $table->enum('article', [
                'canon', 'hp', 'xerox'
            ]);
            $table->integer('sum')->unsigned();
            $table->smallinteger('status')->unsigned();
        });

        Schema::create('order_cartridges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->string('type', 16);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_cartridges');
        Schema::dropIfExists('orders');
    }
}
