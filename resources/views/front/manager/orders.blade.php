@extends('layouts.app')

@section('content')
    <table class='table table-hover table-bordered' id='orders'>
        <thead>
            <tr>
                <th>ID</th>
                <th>Телефон</th>
                <th>ФИО</th>
                <th>Статус</th>
                <th>Корзина</th>
                <th>Сумма</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @include('front.manager.orders-table', ['orders' => $orders])
        </tbody>
    </table>


    <div id="modal-order-edit" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Редактирование</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
        </div>
    </div>

@endsection

@section('js')
<script>
    $('#orders').on('click', '.btn--edit', function(e) {
        e.preventDefault();
        var modal = $('#modal-order-edit');
        $.get("/manager/order/" + $(this).data('id'))
            .done(function(response) {
                modal.modal('show');
                modal.find('.modal-body').html(response);
            });
    });
    $('#modal-order-edit').on('submit', 'form', function(e) {
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var url = form.attr('action');
        form.find('.form-group.has-error').removeClass('has-error');
        form.find('.has-error').text('');
        var page = $('#orders-page').data('page');
        $.ajax({
            method: 'POST',
            url: url + '?page=' + page,
            data: data,
            success: function(response) {
                $('#orders').find('tbody').html(response);
                $('#modal-order-edit').modal('hide');
            },
            error: function(response) {
                var json = response && response.responseJSON;
                if (json && json.errors) {
                    $.each(json.errors, function(index, item) {
                        form.find('[name=' + index + ']')
                            .next('.has-error')
                            .text(item[0])
                            .closest('.form-group')
                            .addClass('has-error');
                    });
                }
            }
        });
    });
</script>
@endsection
