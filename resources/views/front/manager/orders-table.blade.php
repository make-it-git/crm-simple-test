@foreach ($orders as $order)
    <tr>
        <td>{{ $order->id }}</td>
        <td>{{ $order->phone }}</td>
        <td>{{ $order->fio }}</td>
        <td>{{ $order->status_human }}</td>
        <td>
            @foreach ($order->cartridges as $cartridge)
                {{ $cartridge->type }}
            @endforeach
        </td>
        <td>{{ $order->sum }}</td>
        <td>
            <button class='btn btn-default btn--edit' data-id='{{ $order->id }}'>
                Редактировать
            </button>
        </td>
    </tr>
@endforeach
{{ $orders->links() }}
<div id="orders-page" data-page="{{ $orders->currentPage() }}"></div>
