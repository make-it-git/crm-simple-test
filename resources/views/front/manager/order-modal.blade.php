<form method="POST" action="{{ route("front.manager.update-order", ["order" => $order]) }}">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="fio">ФИО:</label>
        <input type="text" class="form-control" id="fio" name="fio" value="{{ $order->fio }}">
        <span class="help-block has-error"></span>
    </div>
    <div class="form-group">
        <label for="phone">Телефон:</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{ $order->phone }}">
        <span class="help-block has-error"></span>
    </div>
    <div class="form-group">
        <label for="phone">Статус:</label>
        <select name="status" class="form-control">
            @foreach (\App\Order::$statuses_translations as $id => $title)
                <option value="{{ $id }}" {{ $id == $order->status ? "selected" : "" }}>
                    {{ $title }}
                </option>
            @endforeach
        </select>
        <span class="help-block has-error"></span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Сохранить</button>
    <div>
</form>
