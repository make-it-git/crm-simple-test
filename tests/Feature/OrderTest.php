<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Cartridge;

class OrderTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreateInvalid()
    {
        $data = [
            'fio' => '   ',
            'phone' => '7(900)900-20201',
            'article' => 'wrong',
            'sum' => '0',
        ];
        $response = $this->post(route('api.order.create'), $data)->json();
        $this->assertEquals(-1, $response['id']);
        $this->assertEquals("The fio field is required.", $response['errors']['fio'][0]);
        $this->assertEquals("The phone has invalid phone format.", $response['errors']['phone'][0]);
        $this->assertEquals("The selected article is invalid.", $response['errors']['article'][0]);
        $this->assertEquals("The sum must be at least 1.", $response['errors']['sum'][0]);
    }

    public function testCreateValid()
    {
        $data = [
            'fio' => 'Ivan',
            'phone' => '7(900)900-2020',
            'article' => 'hp',
            'sum' => '2390',
        ];
        $response = $this->post(route('api.order.create'), $data)->json();
        $this->assertGreaterThanOrEqual(1, $response['id']);
        $this->assertEquals(1, Cartridge::count());
    }

    public function testCreateValid2()
    {
        $data = [
            'fio' => 'Ivan',
            'phone' => '7(900)900-2020',
            'article' => 'canon',
            'sum' => 1000 + 390 + 490
        ];
        $response = $this->post(route('api.order.create'), $data)->json();
        $this->assertGreaterThanOrEqual(1, $response['id']);
        $this->assertEquals(2, Cartridge::count());
    }
}
